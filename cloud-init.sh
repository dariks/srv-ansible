#!/bin/bash
apt-get update && apt-get install git curl wget ansible -y && mkdir -p /opt && cd /opt
git clone https://gitlab.com/dariks/srv-ansible.git /opt/srv-ansible
ansible-playbook -i /opt/srv-ansible/ansible-config/hosts /opt/srv-ansible/playbooks/basic-setup.yml --limit localhost
