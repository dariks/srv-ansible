#!/bin/bash

ROOTDIR="/data/borg-1/"
LOG="prune-backup.log"

BACKUPDIRS="$(ls $ROOTDIR)"

# copy all output to logfile
exec > >(tee -i ${LOG})
exec 2>&1

for backupdir in $BACKUPDIRS; do
echo "###### Pruning backup for $backupdir on $(date) ######"

borg prune -v ${ROOTDIR}${BACKUPDIRS} \
--keep-daily=7 \
--keep-weekly=4 \
--keep-monthly=6
done;

echo "###### Pruning finished ######"

# Send logfile to serveradmin
mailx -r borgsrv@rippen-ks.de -s "Backup | Prune" borgbackup@danielrippen.de < $LOG
