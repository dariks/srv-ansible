#!/bin/bash

##
## Save backup to directory named after hostname
##

LOG="backup.log"
HOST=`hostname`
REPOSITORY="ssh://borgsrv.rippen-ks.de:22//data/borg-1/"$HOST

##
## Write output to logfile
##

exec > >(tee -i ${LOG})
exec 2>&1

echo "###### Starting backup on $(date) ######"


##
## Create list of installed software
##

dpkg --get-selections > /root/backup/software.list


##
## Create database dumps
##

# echo "Creating database dumps ..."
# /bin/bash /root/backup/dbdump.sh


##
## Sync backup data
##

echo "Syncing backup files ..."
borg create -v --stats                                  \
    $REPOSITORY::'{now:%Y-%m-%d_%H:%M}'                 \
    /root/backup                                        \
    /etc                                                \
    /data                                               \
    /var/www                                            \
#    /var/vmail                                          \
#    /storage


echo "###### Finished backup on $(date) ######"


##
## Send mail to admin
##

mailx -a "From: "$HOST" Backup <"$HOST"@rippen-ks.de>" -s "Backup | "$HOST borgbackup@danielrippen.de < $LOG
