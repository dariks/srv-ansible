#!/bin/bash

search="global link"

for i in $search; do
  result="$(ip addr show dev eth0 | grep inet | grep $i | cut -d" " -f 6 | tr "\n" " ")"
  echo "searching $i"
  echo "$result"
  #ip addr show dev eth0 | grep inet | grep $i | cut -d" " -f 6 | tr "\n" " ";
done
