#!/bin/bash
RUNNING_KERNEL=$(uname -r)

hostnamectl | grep -i centos >/dev/null 2>&1
if [ ${PIPESTATUS[1]} -eq 0 ]; then
        CURRENT_KERNEL=$(rpm -q --last kernel | perl -pe 's/^kernel-(\S+).*/$1/' | head -1)
fi

hostnamectl | grep -i debian >/dev/null 2>&1
if [ ${PIPESTATUS[1]} -eq 0 ]; then
        CURRENT_KERNEL=$(dpkg --list | grep linux-image | awk -F'linux-image-' '{print $2}' | grep -E '^[[:digit:]]' | cut -d' ' -f1 | sort -rn | head -1)
fi

hostnamectl | grep -i ubuntu >/dev/null 2>&1
if [ ${PIPESTATUS[1]} -eq 0 ]; then
        CURRENT_KERNEL=$(dpkg --list | grep linux-image | grep ii | awk -F'linux-image-' '{print $2}' | grep -E '^[[:digit:]]' | cut -d' ' -f1 | sort -rn | head -1)
fi

#hostnamectl | grep -i raspbian >/dev/null 2>&1
#if [ ${PIPESTATUS[1]} -eq 0 ]; then
#        CURRENT_KERNEL=$(dpkg --list | grep linux-image | awk -F'linux-image-' '{print $2}' | grep -E '^[[:digit:]]' | cut -d' ' -f1 | sort -rn | head -1)
#fi

hostnamectl | grep -i raspbian >/dev/null 2>&1
if [ ${PIPESTATUS[1]} -eq 0 ]; then
    CURRENT_KERNEL="dummy"
fi

if [ "$RUNNING_KERNEL" != "$CURRENT_KERNEL" ]; then
        echo "current: $CURRENT_KERNEL"
        echo "running: $RUNNING_KERNEL"
        echo "System reboot is required!"
else
        echo "Kernel is uptodate!"
fi

exit 0

