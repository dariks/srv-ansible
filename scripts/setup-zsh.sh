#!/bin/zsh
timeout 15 sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sed -i -e 's~\(ZSH_THEME="\)[^"]*\(".*\)~\1pygmalion\2~' .zshrc
chsh -s /bin/zsh
echo "Set ZSH as default Shell"
